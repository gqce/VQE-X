import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm, rc
import sys

full = []
cyc = []
lin = []


with open('gate.full.new.dat') as infile:
    for line in infile:
        value = line.split()
        if value[0] == '#':continue
        full.append([float(v) for v in value])

with open('gate.cir.new.dat') as infile:
    for line in infile:
        value = line.split()
        if value[0] == '#':continue
        cyc.append([float(v) for v in value])

with open('gate.lin.new.dat') as infile:
    for line in infile:
        value = line.split()
        if value[0] == '#':continue
        lin.append([float(v) for v in value])

full = np.array(full)
cyc = np.array(cyc)
lin = np.array(lin)

#plt.figure(1)
fig, (ax1,ax2) = plt.subplots(2,1)
ax1.set_yscale('log', basey=2)
ax1.errorbar(lin[:3,0], lin[:3,1], lin[:3,2], label='$h_z=0.5$, linear')
ax1.errorbar(cyc[:3,0], cyc[:3,1], cyc[:3,2], label='$h_z=0.5$, cyclic')
ax1.errorbar(full[:3,0], full[:3,1], full[:3,2], label='$h_z=0.5$, full')
ax1.errorbar(lin[3:,0], lin[3:,1], lin[3:,2], label='$h_z=0$, linear')
ax1.errorbar(cyc[3:,0], cyc[3:,1], cyc[3:,2], label='$h_z=0$, cyclic')
#ax1.set_xlabel('$N$', fontsize=18)
ax1.set_ylabel('# of CNOTs', fontsize=18)
ax1.set_xticks([5,6,7,8])
ax1.set_xticklabels(['5','6','7','8'], size=18)
ax1.set_yticks([2**4,2**6,2**8,2**10])
ax1.set_yticklabels(['$2^4$','$2^6$','$2^8$','$2^{10}$'], size=18)
ax1.set_title('Lowest/highest energy states',fontsize=18)
ax1.legend(fontsize=18)

ax2.set_yscale('log', basey=2)
ax2.errorbar(lin[:3,0], lin[:3,3], lin[:3,4], label='$h_z=0.5$, linear')
ax2.errorbar(cyc[:3,0], cyc[:3,3], cyc[:3,4], label='$h_z=0.5$, cyclic')
ax2.errorbar(full[:3,0], full[:3,3], full[:3,4], label='$h_z=0.5$, full')
ax2.errorbar(lin[3:,0], lin[3:,3], lin[3:,4], label='$h_z=0$, linear')
ax2.errorbar(cyc[3:,0], cyc[3:,3], cyc[3:,4], label='$h_z=0$, cyclic')
ax2.set_xlabel('$N$', fontsize=18)
ax2.set_ylabel('# of CNOTs', fontsize=18)
ax2.set_xticks([5,6,7,8])
ax2.set_xticklabels(['5','6','7','8'], size=18)
ax2.set_yticks([2**4,2**6,2**8,2**10])
ax2.set_yticklabels(['$2^4$','$2^6$','$2^8$','$2^{10}$'], size=18)
ax2.set_title('Other excited states',fontsize=18)
#ax1.set_yticks(fontsize=18)
ax1.legend(fontsize=18)

plt.show()
        
