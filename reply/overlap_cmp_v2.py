import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm, rc
import sys

rc('font',**{'family':'sans-serif', 'size' : 10})
rc('text', usetex=True)
plt.rcParams['text.latex.preamble'] = r'\usepackage{mathrsfs}'


#vqe-minimal-hz0.0
eFile = ['./vqe-minimal-hz0-e-5.dat', './vqe-maximal-hz0.5.dat', './vqe-maximal-hz0.5.dat', './vqe-maximal-hz0.5.dat']
#eFile = ['vqe-maximal-hz0.5-e-5.dat', 'vqe-minimal-hz0-e-5.dat']
overlapFile = ['overlap_N6_hz0_e-5.dat', 'overlap_N6_hz0.5_e-4.dat', 'overlap_N6_hz0.5_e-4.dat', 'overlap_N6_hz0.5_dE0.12.dat', ]
#overlapFile = ['overlap_N6_hz0.5_e-4.dat', 'overlap_N6_hz0_e-4.dat']
eigFile = ['./ED-hz0.0.dat', './ED-hz0.5.dat', './ED-hz0.5.dat','./ED-hz0.5.dat']
titles = [r'$h_z = 0$, $\epsilon=10^{-5}$, $\Delta=10^{-8}$',r'$h_z = 0.5$, $\epsilon=10^{-4}$, $\Delta=10^{-8}$', r'$h_z = 0.5$, $\epsilon=10^{-4}$, $\Delta=10^{-8}$', r'$h_z = 0.5$, $\epsilon=10^{-4}$, $\Delta = 0.12$']

labels = ['(a)', '(b)', '(c)', '(d)']

# Read in data for the four panels
energy_1 = []
overlap_1 = []
clen_1 = []
eig_1 = []

energy_2 = []
overlap_2 = []
clen_2 = []
eig_2 = []

energy_3 = []
overlap_3 = []
clen_3 = []
eig_3 = []

energy_4 = []
overlap_4 = []
clen_4 = []
eig_4 = []

# for i, f in enumerate(eFile,1):
with open(eFile[0]) as infile:
    for line in infile:
     values = line.split()
     energy_1.append(float(values[3]))
     clen_1.append(int(values[4]))
with open(overlapFile[0]) as infile:
    for line in infile:
     values = line.split()
     overlap_1.append(float(values[-2]))
with open(eigFile[0]) as infile:
    for line in infile:
      values = line.split()
      eig_1.append(float(values[0]))

with open(eFile[1]) as infile:
    for line in infile:
     values = line.split()
     energy_2.append(float(values[3]))
     clen_2.append(int(values[4]))
with open(overlapFile[1]) as infile:
    for line in infile:
     values = line.split()
     overlap_2.append(float(values[-2]))
with open(eigFile[1]) as infile:
    for line in infile:
      values = line.split()
      eig_2.append(float(values[0]))

with open(eFile[2]) as infile:
    for line in infile:
     values = line.split()
     energy_3.append(float(values[3]))
     clen_3.append(int(values[4]))
with open(overlapFile[2]) as infile:
    for line in infile:
     values = line.split()
     overlap_3.append(float(values[-2]))
with open(eigFile[2]) as infile:
    for line in infile:
      values = line.split()
      eig_3.append(float(values[0]))

with open(eFile[3]) as infile:
    for line in infile:
     values = line.split()
     energy_4.append(float(values[3]))
     clen_4.append(int(values[4]))
with open(overlapFile[3]) as infile:
    for line in infile:
     values = line.split()
     overlap_4.append(float(values[-2]))
with open(eigFile[3]) as infile:
    for line in infile:
      values = line.split()
      eig_4.append(float(values[0]))

# Create figure
# one column in APS = 3.375 in
# two column in APS =
fig = plt.figure(figsize = (3.375, 1.5*2.086))

     #print(min(overlap))
#      xstart = [-7.5, -7.5]
#      xend = [10, 7.5]
#      #for ii in range(len(Z)):
#      #     print(Ef[ii], Z[ii], QFI[ii])
#      #print(' ')
#      plt.subplot(1,2,i)
# ##     sc = plt.scatter(range(len(energy)), energy, c=overlap,
# ##                      vmin=0, vmax=1, s=20)
# ##     cb = plt.colorbar(sc)
# ##     cb.ax.tick_params(labelsize=16)
#      plt.plot(overlap, energy, 'o')
#      for e in eig:
#           plt.plot([0,len(energy)],[e,e], 'gray')
#      plt.xlabel('Overlap', fontsize=18)
#      plt.ylabel('Final energy', fontsize=18)
#      plt.xticks(fontsize=18)
#      #ax.xaxis.set_minor_locator(MultipleLocator(50))
#      plt.ylim([-8, 10])
#      plt.xlim([0.4, 1.2])
#      plt.yticks(np.arange(-8,10,4), fontsize=18)
#      plt.title(title[i-1], fontsize=18)

# First row

ax1 = plt.subplot(2,2,1)
ax2 = plt.subplot(2,2,2, sharey = ax1)

for e in eig_1:
    ax1.plot([0,len(energy_1)],[e,e], 'gray', linewidth = 0.5, alpha=0.6)
for e in eig_2:
    ax2.plot([0,len(energy_2)],[e,e], 'gray', linewidth = 0.5, alpha=0.6)

ax1.plot(overlap_1, energy_1, 'o', markersize = 0.4, , color=color_blue)
ax2.plot(overlap_2, energy_2, 'o', markersize = 0.4, , color=color_blue)

ax1.set_title(titles[0], fontsize = 8)
ax2.set_title(titles[1], fontsize = 8)

ax1.set_xlabel('Overlap', fontsize = 8)
ax1.set_ylabel('Energy $E/J$', fontsize = 8)
ax2.set_xlabel('Overlap', fontsize = 8)

ax1.tick_params(labelsize = 8)
ax2.tick_params(labelsize = 8)
plt.setp(ax2.get_yticklabels(), visible = False)

ax1.set_xlim([0.4, 1.05])
ax2.set_xlim([0.4, 1.05])
ax1.set_ylim([-8, 10])

# ax1.set_xticks(np.arange(0,len(energy_min), 250))
# ax1.set_yticks(np.arange(-8,10,4))
# ax2.set_xticks(np.arange(0,len(energy_max), 250))

# plt.show()

##### EXPORT PLOT INTO PNG FILE #########
#plt.subplots_adjust(wspace=0, hspace=0.)
#plt.tight_layout()
plt.savefig('./fig_overlap.png', format='png', dpi = 600, bbox_inches='tight', pad_inches = 0.01)



