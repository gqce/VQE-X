import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm, rc
import sys

eFile = ['./vqe-maximal-hz0.5.dat', './vqe-maximal-hz0.5.dat']
#eFile = ['vqe-maximal-hz0.5-e-5.dat', 'vqe-minimal-hz0-e-5.dat']
overlapFile = ['overlap_N6_hz0.5_e-4.dat', 'overlap_N6_hz0.5_dE0.12.dat', ]
#overlapFile = ['overlap_N6_hz0.5_e-4.dat', 'overlap_N6_hz0_e-4.dat']
eigFile = ['./ED-hz0.5.dat', './ED-hz0.5.dat']
title = ['$h_z$ = 0.5, $\epsilon=10^{-4}$, $\delta E=10^{-8}$', '$h_z$ = 0, $\epsilon=10^{-4}$, $\delta E = 0.12$']

for i, f in enumerate(eFile,1):
     energy = []
     overlap = []
     clen = []
     eig = []
     with open(f) as infile:
         for line in infile:
             values = line.split()
             energy.append(float(values[3]))
             clen.append(int(values[4]))
     with open(overlapFile[i-1]) as infile:
         for line in infile:
             values = line.split()
             overlap.append(float(values[-2]))
     with open(eigFile[i-1]) as infile:
          for line in infile:
              values = line.split()
              eig.append(float(values[0]))
     #print(min(overlap))
     xstart = [-7.5, -7.5]
     xend = [10, 7.5]
     #for ii in range(len(Z)):
     #     print(Ef[ii], Z[ii], QFI[ii])
     #print(' ')
     plt.subplot(1,2,i)
##     sc = plt.scatter(range(len(energy)), energy, c=overlap,
##                      vmin=0, vmax=1, s=20)
##     cb = plt.colorbar(sc)
##     cb.ax.tick_params(labelsize=16)
     plt.plot(overlap, energy, 'o')
     for e in eig:
          plt.plot([0,len(energy)],[e,e], 'gray')
     plt.xlabel('Overlap', fontsize=18)
     plt.ylabel('Final energy', fontsize=18)
     plt.xticks(fontsize=18)
     #ax.xaxis.set_minor_locator(MultipleLocator(50))
     plt.ylim([-8, 10])
     plt.xlim([0.4, 1.2])
     plt.yticks(np.arange(-8,10,4), fontsize=18)
     plt.title(title[i-1], fontsize=18)

plt.show()

