import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm, rc
import sys

eFile = ['./vqe-maximal-hz0.5.dat', './vqe-minimal-hz0.0.dat']
title = ['$h_z=0.5$, maximal pool', '$h_z=0$, minimal pool']
E_seps = [[], []]

E_seps[0] = [-6.5, -4.5, 0, 4, 8, 20]
E_seps[1] = [-6.5, -4, -1, 1, 4, 6, 20]

for i, f in enumerate(eFile):
     Ei = []
     Ef = []
     #overlap = []
     clen = []
     eig = []
     nbin = len(E_seps[i])
     clenBin = []
     for j in range(nbin):
          clenBin.append([])
     E_ave = np.zeros(nbin)
     clen_ave = np.zeros(nbin)
     clen_std = np.zeros(nbin)
     with open(f) as infile:
         for line in infile:
             values = line.split()
             Ei.append(float(values[2]))
             Ef.append(float(values[3]))
             clen.append(int(values[4]))


     # separate data into nbins based on E_seps
     Ef = np.array(Ef)
     idx = Ef.argsort()
     currentBin = 0
     for j in idx:
          if Ef[j] < E_seps[i][currentBin]: 
               clenBin[currentBin].append(clen[j])
               E_ave[currentBin] += Ef[j]
          else:
               currentBin += 1
               clenBin[currentBin].append(clen[j])
               E_ave[currentBin] += Ef[j]
     for j in range(nbin):
          E_ave[j] /= len(clenBin[j])
          clen_ave[j] = np.mean(clenBin[j])
          clen_std[j] = np.std(clenBin[j])

     clen_ave_exc = 0.
     print(f"len(clen_ave) = {len(clen_ave)}")
     for idx, clen in enumerate(clen_ave):
          if idx > 0 and idx < len(clen_ave) - 1:
               clen_ave_exc = clen_ave_exc + clen
     clen_ave_exc = clen_ave_exc/(len(clen_ave) - 2)

     clen_std_exc = 0.
     print(f"len(clen_std) = {len(clen_std)}")
     for idx, clen in enumerate(clen_std):
          if idx > 0 and idx < len(clen_std) - 1:
               clen_std_exc = clen_std_exc + clen
     clen_std_exc = clen_std_exc/(len(clen_std) - 2)

     plt.subplot(2,1, i+1)
     print(f"file = {f}")
     print(f"E_ave = {E_ave}")
     print(f"clen_ave = {clen_ave}")
     print(f"clen_ave_exc = {clen_ave_exc}")
     print(f" clen_std = {clen_std}")
     print(f"clen_std_exc = {clen_std_exc}")
     plt.errorbar(E_ave, clen_ave, clen_std)
     #plt.title('Q4 '+ str(len(clen_q4)),fontsize=16)
     if i == 1:
         plt.xlabel('Average final energy', fontsize=18)
     plt.ylabel('Average # of parameters', fontsize=18)
     plt.xticks(fontsize=16)
     plt.yticks(fontsize=16)
     plt.title(title[i], fontsize=18)
     

plt.show()

