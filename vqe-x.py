# author Feng Zhang (fzhang@ameslab.gov)
import numpy as np
from scipy.optimize import minimize, minimize_scalar
from itertools import permutations, combinations
import matplotlib.pyplot as plt
import sys

def h_i(i):
    zterm = 1
    xterm = 1
    zsum = 1
    if i > N-1:
        print('error')
        sys.exit(1)
    elif i == N-1:
        zterm = np.kron(Z, zterm)
        for j in range(1,N-1):
            zterm = np.kron(I, zterm)
        zterm = np.kron(Z, zterm)
    else:
        for j in range(i):
            zterm = np.kron(I, zterm)
        zterm = np.kron(Z, np.kron(Z, zterm))
        for j in range(i+2,N):
            zterm = np.kron(I, zterm)
    for j in range(i):
        xterm = np.kron(I, xterm)
    xterm = np.kron(X, xterm)
    for j in range(i+1,N):
        xterm = np.kron(I,xterm)
    for j in range(i):
        zsum = np.kron(I, zsum)
    zsum = np.kron(Z, zsum)
    for j in range(i+1,N):
        zsum = np.kron(I, zsum)
    return zterm + hx*xterm + hz*zsum

def hamil():
    h = np.zeros(ndim, ndim)
    for i in range(N):
        h = np.add(h,h_i(i))
    return h

def getPsi(U, theta, psi0):
    operator = np.cos(theta)*np.eye(ndim) + 1j*np.sin(theta)*U
    return np.dot(operator, psi0)

def expectValue(psi, operator):
    return np.vdot(psi, np.dot(operator, psi))

def commute(A, B):
    return np.dot(A,B) - np.dot(B,A)

def getGradient(psi, U, operator):
    return 1j*expectValue(psi, commute(operator, U))

''' 
cost function for a single operator
'''
def costfunc(theta, U, psi0, h, h2):
    psi = getPsi(U, theta, psi0)
    return expectValue(psi, h2).real - expectValue(psi, h).real**2

''' 
cost function for a list of operators
'''
def costfunc2(theta, U, psi0, h, h2):
    if len(theta) != len(U) : print('error!')
    psi = psi0
    for i in range(len(theta)):
        psi = getPsi(U[i], theta[i], psi)
    return expectValue(psi, h2).real - expectValue(psi, h).real**2

def costGradient(theta, U, psi0, h, h2):
    psi = getPsi(U, theta, psi0)
    g1 = getGradient(psi, U, h2)
    g2 = getGradient(psi, U, h)
    return g1 - 2*expectValue(psi, h)*g2
    
def singleY(ysite):
    U = 1
    for i in range(N):
        if i == ysite : U = np.kron(Y, U)
        else : U = np.kron(I, U)
    return U

def singleYwithX(ysite, xlist):
    U = 1
    for i in range(N):
        if i == ysite : U = np.kron(Y, U)
        elif i in xlist : U = np.kron(X, U)
        else : U = np.kron(I, U)
    return U

def singleYwithZ(ysite, zlist):
    U = 1
    for i in range(N):
        if i == ysite : U = np.kron(Y, U)
        elif i in zlist : U = np.kron(Z, U)
        else : U = np.kron(I, U)
    return U

def get_max_pool():
    Nmax = 2 # up to 2-body operators
    Upool = []
    labelpool = []
    #add single Y's
    for i in range(N):
        Upool.append(singleY(i))
        labelpool.append('Y'+str(i))
    for iy in range(N):
        #add single Y with X's
        xpool = list(range(N))
        xpool.remove(iy)
        for ix in range(1,Nmax):
            for xlist in combinations(xpool, ix):
                paulistr = 'Y'+str(iy)
                for xx in xlist: paulistr += ('X'+str(xx))
                labelpool.append(paulistr)
                Upool.append(singleYwithX(iy, xlist))
        #add single Y with Z's
        zpool = list(range(N))
        zpool.remove(iy)
        for iz in range(1,Nmax):
            for zlist in combinations(zpool, iz):
                paulistr = 'Y'+str(iy)
                for zz in zlist: paulistr += ('Z'+str(zz))
                labelpool.append(paulistr)
                Upool.append(singleYwithZ(iy, zlist))
    return Upool,labelpool

def get_min_pool():
    Nmax = 2 # up to 2-body operators
    Upool = []
    labelpool = []
    #add single Y's
    for i in range(N):
        Upool.append(singleY(i))
        labelpool.append('Y'+str(i))
    for iy in range(N):
        #add single Y with Z's
        if iy < N - 1:
            zlist = [iy + 1]
        else:
            zlist = [0]
        paulistr = 'Y'+str(iy)
        for zz in zlist: paulistr += ('Z'+str(zz))
        labelpool.append(paulistr)
        Upool.append(singleYwithZ(iy, zlist))
    return Upool,labelpool

def ed(hamil):
    w,v = np.linalg.eig(hamil)
    return w,v

def Z_i(i):
    O = 1
    for ii in range(N):
        if ii == i : O = np.kron(Z, O)
        else : O = np.kron(I, O)
    return O

def qfi(psi):
    O = np.zeros((ndim, ndim))
    for i in range(N):
        if i < N/2 : O += .5 * Z_i(i)
        else : O -= .5 * Z_i(i)
    O2 = np.dot(O, O)
    return 4 * (expectValue(psi, O2) - expectValue(psi, O)**2)
        
''' Pauli matrices '''
I = np.array([[1,0],[0,1]])
X = np.array([[0,1],[1,0]])
Y = np.array([[0,-1j],[1j,0]])
Z = np.array([[1,0],[0,-1]])


N = 6 # number of qubits
ndim = np.power(2, N)
hx = 0.8 
hz = 0.0
eps = 1.e-4 
niter = 100

h = hamil()
h2 = np.dot(h, h)

''' initialization '''
psi0 = 1
for i in range(N):
   angle = np.random.random()*2*np.pi
   p = np.array([np.cos(angle),np.sin(angle)])
   psi0 = np.kron(p,psi0)
theta = []
Ulist = []
labellist = []
theta0 = 0
psiCurr = psi0
eCurr = costfunc(theta0,np.eye(ndim),psi0,h,h2)

# implement the minimal operator pool
Upool,labelpool = get_min_pool()
# implement the maximal operator pool
#Upool,labelpool = get_max_pool()

f = open('curr.state', 'w')
print('None 0', eCurr)
for it in range(niter):
    # operator selection based on 1-parameter optimization 
    Emin = 1.e6
    for i in range(len(Upool)):
        res = minimize_scalar(costfunc,bounds=(0,np.pi),
              args=(Upool[i],psiCurr,h,h2), method='Golden',\
              options={'maxiter':10000})
        if res.fun < Emin:
            Emin = res.fun
            iselect = i
            theta_min = res.x
    print(labelpool[iselect], end=' ')
    theta.append(theta_min)
    Ulist.append(Upool[iselect])
    labellist.append(labelpool[iselect])
    # multi-parameter optimization 
    res = minimize(costfunc2, theta, args=(Ulist,psi0,h,h2), method='nelder-mead',
                   options={'xatol':1e-8,'disp':False, 'maxiter':10000})
    theta = list(res.x)
    eCurr = res.fun
    psiCurr = psi0
    for i in range(len(Ulist)):
        psiCurr = getPsi(Ulist[i], theta[i], psiCurr)
    hpsi = np.dot(h, psiCurr)
    nhpsi = np.linalg.norm(hpsi)
    print(it+1, eCurr)
    print(it+1, eCurr, file=f, flush=True)
    for label in labellist : print(label, end = ' ', file=f, flush=True)
    print('', file=f, flush=True)
    for angle in theta: print(angle, end = ' ', file=f, flush=True)
    print('', file=f, flush=True)
    if(nhpsi < eps): # converging to eigenstate with zero energy 
        print('reached convergence: ', expectValue(psi0, h).real, 0, it+1, 
                        qfi(psiCurr).real)
        print('finalstate: ', end=' ')
        for i in range(ndim): print(psiCurr[i].real, end=' ')
        print(flush=True)
        break
    else:
        # check collinearity between $\Psi$ and $H\Psi$
        delta = np.absolute(np.vdot(psiCurr,hpsi))/nhpsi - 1
    if(np.absolute(delta) < eps):
        print('reached convergence: ', expectValue(psi0, h).real,
              expectValue(psiCurr, h).real, it+1, qfi(psiCurr).real)
        print('finalstate: ', end=' ')
        for i in range(ndim): print(psiCurr[i].real, end=' ')
        print(flush=True)
        break
