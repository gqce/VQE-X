[![Paper](https://img.shields.io/badge/paper-arXiv%3A2104.12636-B31B1B.svg)](https://arxiv.org/abs/2104.12636)

[Figshare link](https://doi.org/10.6084/m9.figshare.14916282)

# Adaptive variational quantum eigensolvers for highly excited states
Feng Zhang, Niladri Gomes, Yongxin Yao, [Peter P. Orth](https://faculty.sites.iastate.edu/porth/), Thomas Iadecola

### Description
This repository includes code and data for the adaptive VQE-X method developed in [arXiv:2104.12636](https://arxiv.org/abs/2104.12636).

The adaptive VQE-X algorithm is implemented in `/vqe-x.py`. 
Previous versions of the code are found in `/script/vqe-x.py` and `/script/vqe-x-complete-pool.py`. The two files use the minimal and maximal pool respectively. 

The adaptive folded spectrum method using the variance cost function (see Eq.(10) in the paper) is implemented in the files `/folded_spectrum_src/vqe-x_maxpool.py` and `/folded_spectrum_src/vqe-x_minpool.py`. The two files use the minimal and maximal pool respectively. 

Figures are generated using the various plotting scripts (often named `fig##.py`) using the respective data files provided. All data files needed to produce the figures in the paper are provided. The folder `/reply` contains additional plotting scripts and data files that were produces during the review process. 

### Requirements
  - python=3.7
  - matplotlib
  - numpy
  - scipy

### Support 
This material is based upon work supported by the National Science Foundation under Grant No.~2038010 (T.I.~and P.P.O.). F.Z., N.G. and Y.Y. were supported by the U.S. Department of Energy (DOE), Office of Science, Basic Energy Sciences, Division of Materials Sciences and Engineering, and performed the research at the Ames Laboratory, which is operated for the U.S. DOE by Iowa State University under Contract DE-AC02-07CH11358.
