import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm, rc
import sys
#model = 'old-cf-hz0.3'
#models = ['original', 'no-backwards', 'minimal', 'minimal2']
#titles = ['Original', 'Y < X(Z)', 'Minimal (2N-2)', 'Minimal (2N)']
title = 'N = 8, hz = 0.5, maximal pool'
eigModel = 'N8-hz0.5'
offset = 0
#files = ['eps-1/energy.dat', 'eps-2/energy.dat', 'eps-3/energy.dat', 'eps-4/energy.dat']
#titles = ['$\epsilon=10^{-1}$', '$\epsilon=10^{-2}$', '$\epsilon=10^{-3}$',
#          '$\epsilon=10^{-4}$']
file = 'vqe-maximal-hz0.5-n8.dat'
eig = []
with open('../../eigE/eig_' + eigModel +'.dat') as infile:
    for line in infile:
        values = line.split()
        for v in values: eig.append(float(v))


energy = []
clen = []
with open(file) as infile:
    for line in infile:
         values = line.split()
         energy.append(float(values[3]))
         clen.append(float(values[4]))
ave_clen = np.mean(clen)
plt.subplot(121)
sc = plt.scatter(range(len(energy)), energy, c=clen)
cb = plt.colorbar(sc)
cb.ax.tick_params(labelsize=16)
for e in eig:
     plt.plot([0,len(energy)],[e+offset,e+offset], 'gray')
plt.xlabel('Trial', fontsize=18)
plt.ylabel('Final Energy', fontsize=18)
plt.xticks(fontsize=18)
plt.yticks(range(-10,10,4), fontsize=18)
plt.title(title, fontsize=18)

plt.subplot(122)
xp, bins, p = plt.hist(clen, 20, facecolor='blue', alpha=0.5)
for item in p : item.set_height(item.get_height()/len(clen))
plt.plot([ave_clen, ave_clen],[0,0.47],'g--', linewidth=4.0)
plt.ylim((0,0.2))
plt.xlabel('# of variational operators', fontsize=18)
plt.ylabel('Normalized count', fontsize=18)
plt.xticks(fontsize=18)
plt.yticks(np.arange(0,0.2,0.05),fontsize=18)
     #plt.title(titles[i-1], fontsize=18)
##     plt.subplot(2,2,2*i)
##     plt.hist(ediff,facecolor='blue',alpha=0.5)
##     plt.xlabel('$\Delta E$', fontsize=18)
##     plt.ylabel('Count', fontsize=18)
##     plt.xticks(fontsize=18)
##     plt.yticks(fontsize=18)
     #plt.title(titles[i-1], fontsize=18)

plt.show()

