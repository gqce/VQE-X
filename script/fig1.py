import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm, rc
import sys
#model = 'old-cf-hz0.3'
#models = ['original', 'no-backwards', 'minimal', 'minimal2']
#titles = ['Original', 'Y < X(Z)', 'Minimal (2N-2)', 'Minimal (2N)']
#models = ['original', 'noX', 'noZ', 'minimal2']
##titles = ['hz = 0, complete pool', 'hz = 0.5, complete pool',
##          'hz = 0, minimal pool', 'hz = 0.5, minimal pool']

eFile = ['vqe-maximal-hz0.5.dat', 'vqe-minimal-hz0.0.dat']
zFile = ['spin-maximal-hz0.5.dat', 'spin-minimal-hz0.0.dat']
eigFile = ['ED-hz0.5.dat', 'ED-hz0.0.dat']
title = ['hz = 0.5, maximal pool', 'hz = 0, minimal pool']

for i, f in enumerate(eFile,1):
     Ei = []
     Ef = []
     Z = []
     QFI = []
     clen = []
     eigE = []
     eigZ = []
     eigQFI = []
     with open(f) as infile:
         for line in infile:
             values = line.split()
             Ei.append(float(values[2]))
             Ef.append(float(values[3]))
             clen.append(int(values[4]))
             QFI.append(float(values[-1]))
     with open(zFile[i-1]) as infile:
         for line in infile:
             values = line.split()
             Z.append(np.mean([float(v) for v in values]))
     with open(eigFile[i-1]) as infile:
          for line in infile:
              values = line.split()
              eigE.append(float(values[0]))
              eigZ.append(float(values[1]))
              eigQFI.append(float(values[2]))
     xstart = [-7.5, -7.5]
     xend = [10, 7.5]
     for ii in range(len(Z)):
          print(Ef[ii], Z[ii], QFI[ii])
     print(' ')
     plt.figure(i)
     plt.subplot(1,3,1)
     for e in eigE : plt.plot([xstart[i-1], xend[i-1]], [e, e], 'gray')
     plt.plot(Ei, Ef, 'o', markersize=8)
     plt.xlabel('Initial Energy', fontsize=18)
     plt.ylabel('Final Energy', fontsize=18)
     plt.xticks(np.arange(-8,8.4,4), fontsize=18)
     plt.yticks(fontsize=18)
     if i == 1 : plt.ylim([-7.9,7.2])
     if i == 2 : plt.ylim([-7.9,8.2])
     plt.title(title[i-1], fontsize=18)
     if i == 1 : plt.text(-8, 6.5, '(a)',fontsize=18)
     if i == 2 : plt.text(-8, 7.5, '(a)',fontsize=18)
     
     plt.subplot(1,3,2)
     plt.plot(Z, Ef, 'ro', label='VQE-X', markersize=9)
     plt.plot(eigZ, eigE, 'bs',label='ED', markersize=8)
     #plt.legend(loc='lower right', fontsize=18)
     #plt.xlabel('Final Energy', fontsize=18)
     plt.xlabel('<Z>', fontsize=18)
     plt.xticks(np.arange(-1, 1.1, 0.4), fontsize=18)
     plt.yticks(fontsize=0)
     if i == 1 : plt.ylim([-7.9,7.2])
     if i == 2 : plt.ylim([-7.9,8.2])
     if i == 1 : plt.text(-0.95, 6.5, '(b)', fontsize=18)
     if i == 2 : plt.text(-0.95, 7.5, '(b)', fontsize=18)

     plt.subplot(1,3,3)
     plt.plot(QFI, Ef, 'ro', label='VQE-X', markersize=9)
     plt.plot(eigQFI, eigE, 'bs',label='ED', markersize=8)
     plt.legend(loc='lower right', fontsize=18)
     #plt.xlabel('Final Energy', fontsize=18)
     plt.xlabel('QFI', fontsize=18)
     plt.xticks(np.arange(0, 18, 4), fontsize=18)
     plt.yticks(fontsize=0)
     if i == 1 : plt.ylim([-7.9,7.2])
     if i == 2 : plt.ylim([-7.9,8.2])
     if i == 1 : plt.text(0.5, 6.5, '(c)', fontsize=18)
     if i == 2 : plt.text(-0.5, 7.5, '(c)', fontsize=18)

plt.show()

