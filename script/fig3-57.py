import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm, rc
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
import sys

titles = ['$N$ = 7, $h_z$ = 0, minimal pool',
          '$N$ = 7, $h_z$ = 0.5, maximal pool']
eigModel = ['ED-N7-hz0.dat', 'ED-N7-hz0.5.dat']
offset = 0

files = ['vqe-N7-minimal-hz0.dat', 'vqe-N7-maximal-hz0.5.dat']

eig = [[],[]]
with open(eigModel[0]) as infile:
    for line in infile:
        values = line.split()
        eig[0].append(float(values[0]))
with open(eigModel[1]) as infile:
    for line in infile:
        values = line.split()
        eig[1].append(float(values[0]))

labels = ['(a)', '(b)', '(c)', '(d)']

for i, f in enumerate(files,1):
     energy = []
     clen = []
     with open(f) as infile:
         for line in infile:
             values = line.split()
             energy.append(float(values[3]))
             clen.append(float(values[4]))
     ave_clen = np.mean(clen)
     plt.subplot(2,2,i)
     sc = plt.scatter(range(len(energy)), energy, c=clen,
                      vmin=0, vmax=120, s=20)
     if i == 2:
         cb = plt.colorbar(sc)
         cb.ax.tick_params(labelsize=16)
     for e in eig[i-1]:
          plt.plot([0,len(energy)],[e+offset,e+offset], 'gray')
     plt.xlabel('Trial', fontsize=18)
     if i == 1: plt.ylabel('Final energy', fontsize=18)
     if i == 1: plt.xticks(range(0,len(energy),50),fontsize=18)
     if i == 2: plt.xticks(range(0,len(energy),20),fontsize=18)
     #ax.xaxis.set_minor_locator(MultipleLocator(50))
     #plt.ylim([-8, 10])
     #if i == 1 : plt.yticks(np.arange(-8,10,4), fontsize=18)
     #if i == 2 : plt.yticks(np.arange(-8,10,4), fontsize=18)
     plt.yticks(fontsize=18)
     #if i == 1 : plt.text(-13, 8, labels[i-1], fontsize=18)
     #if i == 2 : plt.text(-13, 8, labels[i-1], fontsize=18)
     plt.title(titles[i-1], fontsize=18)
     plt.subplot(2,2,i+2)
     xp, bins, p = plt.hist(clen,facecolor='blue',alpha=0.5)
     for item in p : item.set_height(item.get_height()/len(clen))
     plt.xlabel('# of variational parameters', fontsize=18)
     if i == 1: plt.ylabel('Normalized count', fontsize=18)
     plt.ylim((0,0.47))
     plt.xticks(fontsize=18)
     if i == 1 : plt.yticks(fontsize=18)
     if i == 2 : plt.yticks(fontsize=0)
     #if i == 1: plt.text(10, 0.42, labels[i+1], fontsize=18)
     #if i == 2: plt.text(5, 0.42, labels[i+1], fontsize=18)
     plt.plot([ave_clen, ave_clen],[0,0.47],'g--', linewidth=4.0)
     print(ave_clen)

plt.show()

