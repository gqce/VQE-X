import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib import cm, rc

#from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import sys

rc('font',**{'family':'sans-serif', 'size' : 10})
rc('text', usetex=True)
#plt.rcParams['text.latex.preamble'] = r'\input{../include/texheader}'
plt.rcParams['text.latex.preamble'] = r'\usepackage{mathrsfs}'

titles = ['$h_z=0.5$, $\\mathscr{P}_{\\rm min}$',
          '$h_z=0.5$, $\\mathscr{P}_{\\rm max}$']

eigModel = 'ED-hz0.5.dat'

files = ['vqe-minimal-hz0.5.dat', 'vqe-maximal-hz0.5.dat']

eig = []
with open(eigModel) as infile:
    for line in infile:
        values = line.split()
        eig.append(float(values[0]))

labels = ['(a)', '(b)', '(c)', '(d)']

# one column in APS = 3.375 in
# two column in APS =
fig = plt.figure(figsize = (3.375, 1.5*2.086)) # Golden ratio = 1.62, 3.375/GoldenRatio = 2.086

ax1 = plt.subplot(2,2,1)
ax2 = plt.subplot(2,2,2, sharey = ax1)

# First row: scatter plots

energy_min = []
energy_max = []
clen_min = []
clen_max = []

with open(files[0]) as infile:
	for line in infile:
		values = line.split()
		energy_min.append(float(values[3]))
		clen_min.append(float(values[4]))
	ave_clen_min = np.mean(clen_min)

with open(files[1]) as infile:
	for line in infile:
		values = line.split()
		energy_max.append(float(values[3]))
		clen_max.append(float(values[4]))
	ave_clen_max = np.mean(clen_max)

for e in eig:
	ax1.plot([0,len(energy_min)],[e,e], 'gray', linewidth = 0.5, alpha=0.6)
	ax2.plot([0,len(energy_max)],[e,e], 'gray', linewidth = 0.5, alpha=0.6)

sc_min = ax1.scatter(range(len(energy_min)), energy_min, c=clen_min,
                      vmin=0, vmax=100, s=1, alpha = 1.0, cmap='viridis')
sc_max = ax2.scatter(range(len(energy_max)), energy_max, c=clen_max,
                      vmin=0, vmax=100, s=1, alpha = 1.0, cmap='viridis')

cb = plt.colorbar(sc_max, shrink = 1, orientation = 'vertical')
cb.ax.tick_params(labelsize=7)
cb.ax.set_title('$\\mathcal{N}_{\\rm c}$', fontsize = 6)


ax1.set_xlabel('Trial', fontsize = 8)
ax1.set_ylabel('Energy $E/J$', fontsize = 8)
ax2.set_xlabel('Trial', fontsize = 8)


ax1.text(0,10.8, labels[0], fontsize = 8)
ax2.text(0,10.8, labels[1], fontsize = 8)

# ax1.text(250, 8, titles[0], fontsize = 8)
# ax2.text(250, 8, titles[1], fontsize = 8)

ax1.set_title(titles[0], fontsize = 8)
ax2.set_title(titles[1], fontsize = 8)

ax1.tick_params(labelsize = 8)
ax2.tick_params(labelsize = 8)
plt.setp(ax2.get_yticklabels(), visible = False)
ax1.set_ylim([-8, 13])
ax1.set_xticks(np.arange(0,len(energy_min), 50))
ax1.set_yticks(np.arange(-8,13,4))
ax2.set_xticks(np.arange(0,len(energy_max), 200))

# Second row: histograms
ax3 = plt.subplot(2,2,3)
ax4 = plt.subplot(2,2,4, sharey = ax3)

ax3.tick_params(labelsize = 8)
ax4.tick_params(labelsize = 8)
plt.setp(ax4.get_yticklabels(), visible = False)

ax3.set_xticks(np.arange(0,101, 25))
ax4.set_xticks(np.arange(0,101, 25))

ax3.set_xlabel('$\\mathcal{N}_{\\rm c}$', fontsize = 8)
ax3.set_ylabel('Normalized count', fontsize = 8)
ax4.set_xlabel('$\\mathcal{N}_{\\rm c}$', fontsize = 8)

ax3.text(0,0.37, labels[2], fontsize = 8)
ax4.text(0,0.37, labels[3], fontsize = 8)

counts, bins = np.histogram(clen_min, bins = 10, range = (0, 100))
counts = counts/np.sum(counts)

ax3.hist(bins[:-1], bins, weights=counts)

counts, bins = np.histogram(clen_max, bins = 10, range = (0, 100))
counts = counts/np.sum(counts)
ax4.hist(bins[:-1], bins, weights=counts)

ax3.plot([ave_clen_min, ave_clen_min],[0,0.4],'g--', linewidth=1.0)
ax4.plot([ave_clen_max, ave_clen_max],[0,0.4],'g--', linewidth=1.0)

plt.tight_layout()
#plt.show()
plt.savefig('./fig-hz_0.5.png', format='png', dpi = 600, bbox_inches='tight')
