import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib import cm, rc

#from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import sys

color_red = (0.73, 0.13869999999999993, 0.)
color_orange = (1., 0.6699999999999999, 0.)
color_green = (0.14959999999999996, 0.43999999999999995, 0.12759999999999994)
color_blue = (0.06673600000000002, 0.164512, 0.776)
color_purple = (0.25091600000000003, 0.137378, 0.29800000000000004)
color_ocker = (0.6631400000000001, 0.71, 0.1491)
color_pink = (0.71, 0.1491, 0.44730000000000003)
color_brown = (0.651, 0.33331200000000005, 0.054683999999999955)

color_all = [color_red, color_orange, color_green, color_blue, color_brown, color_pink,color_purple, color_ocker]

rc('font',**{'family':'sans-serif', 'size' : 10})
rc('text', usetex=True)
#plt.rcParams['text.latex.preamble'] = r'\input{../include/texheader}'
plt.rcParams['text.latex.preamble'] = r'\usepackage{mathrsfs}'

eFile = 'vqe-maximal-hz0.5.dat'
zFile = 'spin-maximal-hz0.5.dat'
eigFile = 'ED-hz0.5.dat'
eigEntangleFile = 'EntanglementEntropy-ED-N6-hz0.5.txt'
entangleFile = 'EntanglementEntropy-VQE-X-N6-hz0.5.txt'
#bineigEntangleFile = 'BinAvg-EntanglementEntropy-ED-N6-hz0.5.csv'
#binentangleFile = 'BinAvg-EntanglementEntropy-VQE-X-N6-hz0.5.csv'

title = '$h_z = 0.5$, $\\mathscr{P}_{\\rm max}$'

Ei = []
Ef = []
Z = []
QFI = []
clen = []
eigE = []
eigZ = []
eigQFI = []
eigEntangle_x = []
eigEntangle_y = []
entangle_x = []
entangle_y = []

binEntangleED = np.genfromtxt('BinAvg-EntanglementEntropy-ED-N6-hz0.5.csv', delimiter = ',')
binEntangleVQE = np.genfromtxt('BinAvg-EntanglementEntropy-VQE-X-N6-hz0.5.csv', delimiter = ',')
# print(binEntangleED)
# print(binEntangleED[:,0])
# print(binEntangleED[:,1])

with open(eFile) as infile:
    for line in infile:
        values = line.split()
        Ei.append(float(values[2]))
        Ef.append(float(values[3]))
        clen.append(int(values[4]))
        QFI.append(float(values[-1]))
with open(zFile) as infile:
    for line in infile:
        values = line.split()
        Z.append(np.mean([float(v) for v in values]))
with open(eigFile) as infile:
     for line in infile:
         values = line.split()
         eigE.append(float(values[0]))
         eigZ.append(float(values[1]))
         eigQFI.append(float(values[2]))
with open(entangleFile) as infile:
     for line in infile:
          values = line.split()
          entangle_x.append(float(values[0]))
          entangle_y.append(float(values[1]))
with open(eigEntangleFile) as infile:
     for line in infile:
          values = line.split()
          eigEntangle_x.append(float(values[0]))
          eigEntangle_y.append(float(values[1]))

fig = plt.figure(figsize = (3.375, 2.086)) # Golden ratio = 1.62, 3.375/GoldenRatio = 2.086

ax1 = plt.subplot(1,3,1)


xstart = -7.5
xend = 10.5
for e in eigE :
	ax1.plot([xstart, xend], [e, e], 'gray', linewidth = 0.5, alpha=0.6)

ax1.plot(Ei, Ef, 'o', markersize=0.4, alpha = 1.0, color=color_brown)

ax1.set_xlabel('$E_0/J$', fontsize=8)
ax1.set_ylabel('$E/J$', fontsize=8)

ax1.set_xticks(np.arange(-8,8.4,8))
ax1.set_xticks(np.arange(-8, 8, 4), minor = True)
ax1.set_yticks(np.arange(-5,10.1,5))
ax1.set_yticks(np.arange(-7.5,10, 2.5), minor = True)
ax1.tick_params(labelsize = 8)

#ax1.set_ylim([-7.9,12])
#ax1.set_title(title, fontsize=8)
ax1.text(-8, 8, '(a)',fontsize=8)

# ax2: Magnetization
ax2 = plt.subplot(1,3,2, sharey = ax1)

ax2.plot(eigZ, eigE, color = color_blue, marker = 's', linestyle = 'None', alpha = 1.0, label='ED', markersize=0.8)
ax2.plot(Z, Ef, color = color_red, marker = 'o',  linestyle = 'None', alpha = 1.0, label='VQE-X', markersize=0.5)

ax2.set_xlabel('$M_Z$', fontsize=8)
plt.setp(ax2.get_yticklabels(), visible = False)

ax2.set_xticks(np.arange(-1.,1.1,1))
ax2.set_xticks(np.arange(-1,1,0.25), minor = True)
ax2.tick_params(labelsize = 8)

ax2.text(-0.95, 9, '(b)',fontsize=8)

# ax3: von Neumann entropy
ax3 = plt.subplot(1,3,3, sharey = ax1)
ax3.plot(eigEntangle_y, eigEntangle_x, color = color_blue, marker = 's', linestyle = 'None', alpha = 1.0, label='ED', markersize=1)
ax3.plot(entangle_y, entangle_x, color = color_red, marker = 'o',  linestyle = 'None', alpha = 1.0, label='VQE-X', markersize=0.5)
ax3.plot(binEntangleED[:,1], binEntangleED[:,0], color = color_blue, linestyle = 'solid', linewidth = 0.5)
ax3.plot(binEntangleVQE[:,1], binEntangleVQE[:,0], color = color_red, linestyle = 'solid', linewidth = 0.5)

ax3.set_xlabel('$S_A$', fontsize=8)
plt.setp(ax3.get_yticklabels(), visible = False)

ax3.set_xlim(-0.05,2.7)
ax3.set_xticks(np.arange(0,2.1,1))
ax3.set_xticks(np.arange(0,2,0.5), minor = True)
ax3.tick_params(labelsize = 8)
ax3.legend(loc = 'upper right', fontsize = 6, frameon = True, borderpad  = 0.2, markerfirst = True)

ax3.text(2, 5, '(c)',fontsize=8)

plt.tight_layout()
plt.savefig('./fig-1_v2.png', format='png', dpi = 600, bbox_inches='tight')
