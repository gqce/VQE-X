import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib import cm, rc

import sys

color_red = (0.73, 0.13869999999999993, 0.)
color_orange = (1., 0.6699999999999999, 0.)
color_green = (0.14959999999999996, 0.43999999999999995, 0.12759999999999994)
color_blue = (0.06673600000000002, 0.164512, 0.776)
color_purple = (0.25091600000000003, 0.137378, 0.29800000000000004)
color_ocker = (0.6631400000000001, 0.71, 0.1491)
color_pink = (0.71, 0.1491, 0.44730000000000003)
color_brown = (0.651, 0.33331200000000005, 0.054683999999999955)

color_all = [color_red, color_orange, color_green, color_blue, color_brown, color_pink,color_purple, color_ocker]

rc('font',**{'family':'sans-serif', 'size' : 10})
rc('text', usetex=True)
#plt.rcParams['text.latex.preamble'] = r'\input{../include/texheader}'
plt.rcParams['text.latex.preamble'] = r'\usepackage{mathrsfs}'

data_scaling_hz_0 = np.genfromtxt('size_scaling_hz0.dat', comments = '#')
data_scaling_hz_0_5 = np.genfromtxt('size_scaling_hz0.5.dat', comments = '#')
print(data_scaling_hz_0_5)

fig = plt.figure(figsize = (3.375, 2.086))

plt.errorbar(data_scaling_hz_0[:,0], data_scaling_hz_0[:,1], yerr = data_scaling_hz_0[:,2], label = '$h_z = 0$', color = color_red, linewidth = 1, fmt = 'o-', elinewidth=1.5, capsize=2) # ecolor='lightgray',
plt.errorbar(data_scaling_hz_0_5[:,0], data_scaling_hz_0_5[:,1], yerr = data_scaling_hz_0_5[:,2], label = '$h_z = 0.5$', color = color_blue, linewidth = 1, fmt = 'o-',  elinewidth=1.5, capsize=2)

plt.xlabel('$N$')
plt.ylabel(r'$\langle \mathcal{N}_c \rangle$')

plt.legend(loc = 'best')

plt.tight_layout()
plt.savefig('./fig_scaling_v1.png', format='png', dpi = 600, bbox_inches='tight')



