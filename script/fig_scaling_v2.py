import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib import cm, rc

import sys

color_red = (0.73, 0.13869999999999993, 0.)
color_orange = (1., 0.6699999999999999, 0.)
color_green = (0.14959999999999996, 0.43999999999999995, 0.12759999999999994)
color_blue = (0.06673600000000002, 0.164512, 0.776)
color_purple = (0.25091600000000003, 0.137378, 0.29800000000000004)
color_ocker = (0.6631400000000001, 0.71, 0.1491)
color_pink = (0.71, 0.1491, 0.44730000000000003)
color_brown = (0.651, 0.33331200000000005, 0.054683999999999955)

color_all = [color_red, color_orange, color_green, color_blue, color_brown, color_pink,color_purple, color_ocker]

rc('font',**{'family':'sans-serif', 'size' : 10})
rc('text', usetex=True)
#plt.rcParams['text.latex.preamble'] = r'\input{../include/texheader}'
plt.rcParams['text.latex.preamble'] = r'\usepackage{mathrsfs}'

######## READ IN DATA FOR PANEL A ############

data_scaling_hz_0 = np.genfromtxt('size_scaling_hz0.dat', comments = '#')
data_scaling_hz_0_5 = np.genfromtxt('size_scaling_hz0.5.dat', comments = '#')
print(data_scaling_hz_0_5)

######## READ IN DATA FOR PANEL B #####

full = []
cyc = []
lin = []

with open('../reply/gate.full.new.dat') as infile:
    for line in infile:
        value = line.split()
        if value[0] == '#':continue
        full.append([float(v) for v in value])

with open('../reply/gate.cir.new.dat') as infile:
    for line in infile:
        value = line.split()
        if value[0] == '#':continue
        cyc.append([float(v) for v in value])

with open('../reply/gate.lin.new.dat') as infile:
    for line in infile:
        value = line.split()
        if value[0] == '#':continue
        lin.append([float(v) for v in value])

full = np.array(full)
cyc = np.array(cyc)
lin = np.array(lin)


#### PLOTTING ######

fig = plt.figure(figsize = (3.375, 2*2.086))

###### PLOT PANEL A ####

ax1 = plt.subplot(2,1,1)

ax1.set_yscale('log', basey=2)

ax1.errorbar(data_scaling_hz_0[:,0], data_scaling_hz_0[:,1], yerr = data_scaling_hz_0[:,2], label = '$h_z = 0$', color = color_red, linewidth = 1, fmt = 'o-', elinewidth=1.5, capsize=2) # ecolor='lightgray',
ax1.errorbar(data_scaling_hz_0_5[:,0], data_scaling_hz_0_5[:,1], yerr = data_scaling_hz_0_5[:,2], label = '$h_z = 0.5$', color = color_blue, linewidth = 1, fmt = 'o-',  elinewidth=1.5, capsize=2)

ax1.grid(which = 'major', linestyle = '-', linewidth = 1., alpha = 0.4)
ax1.grid(which = 'minor', linestyle = '-', linewidth = 1., alpha = 0.2)

#ax1.set_xlabel('$N$')
ax1.set_ylabel(r'$\langle \mathcal{N}_c \rangle$')

ax1.set_xticks([5,6,7,8])
ax1.set_xticklabels(['5','6','7','8'])
ax1.set_yticks([2**4,2**5,2**6,2**7])
ax1.set_yticklabels(['$2^4$','$2^5$','$2^6$','$2^7$'])

ax1.text(4.3,1.5*2**7, '(a)')

ax1.legend(loc = 'best', fontsize = 'x-small', markerscale = 0.6)

###### PLOT PANEL B ####

ax2 = plt.subplot(2,1,2, sharex = ax1)

ax2.set_yscale('log', basey=2)

ax2.errorbar(cyc[3:,0], cyc[3:,3], cyc[3:,4], color=color_red, linestyle = '-', linewidth = 1, fmt = 'o-', elinewidth=1.0, capsize=2, markersize = 1.5, label='$h_z=0$, NN, PBC')
ax2.errorbar(lin[3:,0], lin[3:,3], lin[3:,4], color=color_red, linestyle = 'dashed', linewidth = 1, fmt = 'o-', elinewidth=1.0, capsize=2.0, markersize = 1.5, label='$h_z=0$, NN, OBC')
ax2.errorbar(full[:3,0], full[:3,3], full[:3,4], color=color_blue, linestyle = 'dotted', linewidth = 1, fmt = 'o-', elinewidth=1.0, capsize=2.0, markersize = 1.5, label='$h_z=0.5$, all-to-all')
ax2.errorbar(lin[:3,0], lin[:3,3], lin[:3,4], color=color_blue, linestyle = 'dashed', linewidth = 1, fmt = 'o-', elinewidth=1.0, capsize=2.0, markersize = 1.5, label='$h_z=0.5$, NN, OBC')
ax2.errorbar(cyc[:3,0], cyc[:3,3], cyc[:3,4], color=color_blue, linestyle = '-', linewidth = 1, fmt = 'o-', elinewidth=1.0, capsize=2.0, markersize = 1.5, label='$h_z=0.5$, NN, PBC')

ax2.set_xlabel('$N$')
ax2.set_ylabel(r'\# of CNOTs')
#ax2.set_ylim(2**4, 2**14)
ax2.set_xticks([5,6,7,8])
ax2.set_xticklabels(['5','6','7','8'])
ax2.set_yticks([2**4,2**6,2**8,2**10])
ax2.set_yticklabels(['$2^4$','$2^6$','$2^8$','$2^{10}$'])
#ax2.set_title('Other excited states',fontsize=18)
#ax1.set_yticks(fontsize=18)
ax2.grid(which = 'major', linestyle = '-', linewidth = 1., alpha = 0.4)
ax2.grid(which = 'minor', linestyle = '-', linewidth = 1., alpha = 0.2)
ax2.legend(loc='lower left', bbox_to_anchor = (0.0, 1), fontsize = 'x-small', ncol=2, markerscale = 0.6)
#loc='upper left', bbox_to_anchor = (1.05, 1), fancybox = True, shadow = True,

ax2.text(4.3,1.3*2**14, '(b)')

##### EXPORT PLOT INTO PNG FILE #########
plt.subplots_adjust(wspace=0, hspace=0.7)
#plt.tight_layout()
plt.savefig('./fig_scaling_v2.png', format='png', dpi = 600, bbox_inches='tight', pad_inches = 0.01)



