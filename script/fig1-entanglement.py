import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm, rc
import sys
#model = 'old-cf-hz0.3'
#models = ['original', 'no-backwards', 'minimal', 'minimal2']
#titles = ['Original', 'Y < X(Z)', 'Minimal (2N-2)', 'Minimal (2N)']
#models = ['original', 'noX', 'noZ', 'minimal2']
##titles = ['hz = 0, complete pool', 'hz = 0.5, complete pool',
##          'hz = 0, minimal pool', 'hz = 0.5, minimal pool']

eFile = 'vqe-maximal-hz0.5.dat'
zFile = 'spin-maximal-hz0.5.dat'
eigFile = 'ED-hz0.5.dat'
eigEntangleFile = 'EntanglementEntropy-ED-N6-hz0.5.txt'
entangleFile = 'EntanglementEntropy-VQE-X-N6-hz0.5.txt'
title = '$h_z$ = 0.5, maximal pool'

Ei = []
Ef = []
Z = []
QFI = []
clen = []
eigE = []
eigZ = []
eigQFI = []
eigEntangle_x = []
eigEntangle_y = []
entangle_x = []
entangle_y = []
with open(eFile) as infile:
    for line in infile:
        values = line.split()
        Ei.append(float(values[2]))
        Ef.append(float(values[3]))
        clen.append(int(values[4]))
        QFI.append(float(values[-1]))
with open(zFile) as infile:
    for line in infile:
        values = line.split()
        Z.append(np.mean([float(v) for v in values]))
with open(eigFile) as infile:
     for line in infile:
         values = line.split()
         eigE.append(float(values[0]))
         eigZ.append(float(values[1]))
         eigQFI.append(float(values[2]))
with open(entangleFile) as infile:
     for line in infile:
          values = line.split()
          entangle_x.append(float(values[0]))
          entangle_y.append(float(values[1]))
with open(eigEntangleFile) as infile:
     for line in infile:
          values = line.split()
          eigEntangle_x.append(float(values[0]))
          eigEntangle_y.append(float(values[1]))

xstart = -7.5
xend = 10

plt.subplot(1,3,1)
for e in eigE : plt.plot([xstart, xend], [e, e], 'gray')
plt.plot(Ei, Ef, 'o', markersize=8)
plt.xlabel('Initial Energy', fontsize=18)
plt.ylabel('Final Energy', fontsize=18)
plt.xticks(np.arange(-8,8.4,4), fontsize=18)
plt.yticks(fontsize=18)
plt.ylim([-7.9,7.2])
plt.title(title, fontsize=18)
plt.text(-8, 6.5, '(a)',fontsize=18)

plt.subplot(1,3,2)
plt.plot(eigZ, eigE, 'bs',label='ED', markersize=10)
plt.plot(Z, Ef, 'ro', label='VQE-X', markersize=6)
#plt.legend(loc='lower right', fontsize=18)
#plt.xlabel('Final Energy', fontsize=18)
plt.xlabel('$M_Z$', fontsize=18)
plt.xticks(np.arange(-1, 1.1, 0.4), fontsize=18)
plt.yticks(fontsize=0)
plt.ylim([-7.9,7.2])
plt.text(-0.95, 6.5, '(b)', fontsize=18)

plt.subplot(1,3,3)
plt.plot(eigEntangle_y, eigEntangle_x, 'bs',label='ED', markersize=10)
plt.plot(entangle_y, entangle_x, 'ro', label='VQE-X', markersize=6)
plt.legend(loc='upper right', fontsize=18, borderpad=0.3)
#plt.xlabel('Final Energy', fontsize=18)
plt.xlabel('Entanglement entropy', fontsize=18)
plt.xticks(np.arange(0,2,0.4), fontsize=18)
plt.yticks(fontsize=0)
plt.ylim([-7.9,7.2])
plt.text(-0.01, 6.5, '(c)', fontsize=18)

plt.show()

