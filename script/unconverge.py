import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm, rc
import sys
#model = 'old-cf-hz0.3'
#models = ['original', 'no-backwards', 'minimal', 'minimal2']
#titles = ['Original', 'Y < X(Z)', 'Minimal (2N-2)', 'Minimal (2N)']
#title = 'N = 8, hz = 0.5, maximal pool'
eigModel = 'N7-hz0.5'
offset = 0
#files = ['eps-1/energy.dat', 'eps-2/energy.dat', 'eps-3/energy.dat', 'eps-4/energy.dat']
#titles = ['$\epsilon=10^{-1}$', '$\epsilon=10^{-2}$', '$\epsilon=10^{-3}$',
#          '$\epsilon=10^{-4}$']
file = 'N7-unconverge-hz0.5.dat'
eig = []
with open('ED-' + eigModel +'.dat') as infile:
    for line in infile:
        values = line.split()
        #for v in values: eig.append(float(v))
        eig.append(float(values[0]))


energy = []
clen = []
overlap = []
with open(file) as infile:
    for line in infile:
         values = line.split()
         energy.append(float(values[1]))
         clen.append(float(values[2]))
         overlap.append(np.log10(float(values[3])))
ave_clen = np.mean(clen)
sc = plt.scatter(range(len(energy)), energy, c=overlap)
cb = plt.colorbar(sc)
cb.ax.tick_params(labelsize=16)
for e in eig:
     plt.plot([0,len(energy)],[e+offset,e+offset], 'gray')
plt.xlabel('Trial', fontsize=18)
plt.ylabel('Final Energy', fontsize=18)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
#plt.title(title, fontsize=18)



plt.show()

