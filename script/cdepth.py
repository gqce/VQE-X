import pdb
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import rc
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import copy
from scipy import linalg as SciLA
import re

def get_ex(fname, numsteps = False):
    f0 = open(fname,"r")
    junk = f0.readline()
    print(e_list[0])
    e0 = []
    while True:
        line = f0.readline().strip()
        if len(line)<1:
            break
        if 'lambda' in line:
            line = line.split()
            if numsteps:
                e0.append([ float(line[3]),float(line[-2]),float(line[-1]) ])   
            else:
                e0.append([ float(line[3]),float(line[-1])])   
            #e0.append(float(line[-1]))   
            print(line)
            
    return np.asarray(e0)
   

if __name__=="__main__":
    fname1 = "trial_hx0.5hz0.0_dl0.5.out"
    fname2 = "trial_hx0.5hz0.0_dl1.0.out"
    title = r"$N=6; h_{x}=0.8, h_{z}=0.0, \lambda \in [-7,7], d\lambda = 0.5  $"
    #get_param(fname)
    e_list = [-7.04880448, -6.95713015, -5.03144242,\
              -4.72409987, -4.72409987, -3.47887375,\
              -3.47887375, -3.47887375, -3.47887375,\
              -3.43303028, -3.43303028, -3.29106959,\
              -2.95713015, -2.56124969, -2.56124969,\
              -2.56124969, -2.56124969, -2.        ,\
              -2.        , -2.        , -2.        ,\
              -1.92630509, -1.52409987, -1.52409987,\
              -1.00868103, -1.00868103, -1.00868103,\
              -1.00868103, -0.70893041, -0.23303028,\
              -0.23303028, -0.09105697,  0.09105697,\
               0.23303028,  0.23303028,  0.70893041,\
               1.00868103,  1.00868103,  1.00868103,\
               1.00868103,  1.52409987,  1.52409987,\
               1.92630509,  2.        ,  2.        ,\
               2.        ,  2.        ,  2.56124969,\
               2.56124969,  2.56124969,  2.56124969,\
               2.95713015,  3.29106959,  3.43303028,\
               3.43303028,  3.47887375,  3.47887375,\
               3.47887375,  3.47887375,  4.72409987,\
               4.72409987,  5.03144242,  6.95713015,  7.04880448]

    e0 = get_ex(fname1, numsteps = True)
    e1 = get_ex(fname2, numsteps = True)

    fig, ax = plt.subplots()
    for e_x in e_list:
        ax.plot([e_x,e_x],[0,100], 'k--', lw = 0.5)
    # plot data
    #for i, e_x in enumerate(e1):
    #    ax.plot([1,1],[e_x,e_x], 'o')
    for i, e_x in enumerate(e0[:,0]):
        ax.plot([e_x,e_x],[10,10], 'o')
 
    #ax.plot(e0[:,0],e0[:,1],'s')
    #ax.bar(e0[:,0],e0[:,1])
    ax.bar(e0[:,0],e0[:,1])
    #ax.bar(e0[:,0],e0[:,2])

    #ax.set_xlabel("Trials")
    ax.set_xlabel(r"$E$")
    #ax.set_ylabel(r"$CX$, N$_{op}$")
    ax.set_ylabel(r" N$_{op}$")
    labels = [item.get_text() for item in ax.get_xticklabels()]
    #labels[2] = r'$d\lambda$ = 0.5'
    #labels[3] = r'$d\lambda$ = 1.0'

    #ax.set_xticklabels(labels)
    ax.set_title(title)
    plt.show(block=False)
    try:
        input("Hit Enter to close")
    except:
        plt.close()

