import numpy as np
from scipy.optimize import minimize, minimize_scalar
from itertools import permutations, combinations
import matplotlib.pyplot as plt
import sys

I = np.array([[1,0],[0,1]])
X = np.array([[0,1],[1,0]])
Y = np.array([[0,-1j],[1j,0]])
Z = np.array([[1,0],[0,-1]])
N = int(sys.argv[1]) # number of qubits
ndim = np.power(2, N)
hx = 0.8 
hz = 0.0
def h_i(i):
    zterm = 1
    xterm = 1
    zsum = 1
    if i > N-1:
        print('error')
        sys.exit(1)
    elif i == N-1:
        zterm = np.kron(Z, zterm)
        for j in range(1,N-1):
            zterm = np.kron(I, zterm)
        zterm = np.kron(Z, zterm)
    else:
        for j in range(i):
            zterm = np.kron(I, zterm)
        zterm = np.kron(Z, np.kron(Z, zterm))
        for j in range(i+2,N):
            zterm = np.kron(I, zterm)
    for j in range(i):
        xterm = np.kron(I, xterm)
    xterm = np.kron(X, xterm)
    for j in range(i+1,N):
        xterm = np.kron(I,xterm)
    for j in range(i):
        zsum = np.kron(I, zsum)
    zsum = np.kron(Z, zsum)
    for j in range(i+1,N):
        zsum = np.kron(I, zsum)
    return zterm + hx*xterm + hz*zsum

def hamil():
    h = np.zeros(ndim, ndim)
    for i in range(N):
        h = np.add(h,h_i(i))
    return h

def getPsi(U, theta, psi0):
    operator = np.cos(theta)*np.eye(ndim) + 1j*np.sin(theta)*U
    return np.dot(operator, psi0)

def expectValue(psi, operator):
    return np.vdot(psi, np.dot(operator, psi))

def commute(A, B):
    return np.dot(A,B) - np.dot(B,A)

# calculate d/dtheta(<psi0|exp(-ithetaU+)Oexp(ithetaU)|psi0>
# psi = exp(ithetaU)psi0
def getGradient(psi, U, operator):
    return 1j*expectValue(psi, commute(operator, U))

# cost function for single U
def costfunc(theta, U, psi0, h, h2):
    psi = getPsi(U, theta, psi0)
    return expectValue(psi, h2).real - expectValue(psi, h).real**2
    #return (expectValue(psi, h) - lambda) ** 2

# cost function for a list of U
def costfunc2(theta, U, psi0, h, h2):
    if len(theta) != len(U) : print('error!')
    psi = psi0
    for i in range(len(theta)):
        psi = getPsi(U[i], theta[i], psi)
    return expectValue(psi, h2).real - expectValue(psi, h).real**2

def costGradient(theta, U, psi0, h, h2):
    psi = getPsi(U, theta, psi0)
    g1 = getGradient(psi, U, h2)
    g2 = getGradient(psi, U, h)
    return g1 - 2*expectValue(psi, h)*g2
    
def singleY(ysite):
    U = 1
    for i in range(N):
        if i == ysite : U = np.kron(Y, U)
        else : U = np.kron(I, U)
    return U

def singleYwithX(ysite, xlist):
    U = 1
    for i in range(N):
        if i == ysite : U = np.kron(Y, U)
        elif i in xlist : U = np.kron(X, U)
        else : U = np.kron(I, U)
    return U

def singleYwithZ(ysite, zlist):
    U = 1
    for i in range(N):
        if i == ysite : U = np.kron(Y, U)
        elif i in zlist : U = np.kron(Z, U)
        else : U = np.kron(I, U)
    return U

def get_Upool():
    Nmax = 2 # up to 2-body operators
    Upool = []
    labelpool = []
    #add single Y's
    for i in range(N):
        Upool.append(singleY(i))
        labelpool.append('Y'+str(i))
    for iy in range(N):
        #add single Y with Z's
        if iy < N - 1:
            zlist = [iy + 1]
        else:
            zlist = [0]
        paulistr = 'Y'+str(iy)
        for zz in zlist: paulistr += ('Z'+str(zz))
        labelpool.append(paulistr)
        Upool.append(singleYwithZ(iy, zlist))
    return Upool,labelpool

def ed(hamil):
    w,v = np.linalg.eig(hamil)
    return w,v

def Z_i(i):
    O = 1
    for ii in range(N):
        if ii == i : O = np.kron(Z, O)
        else : O = np.kron(I, O)
    return O

def qfi(psi):
    O = np.zeros((ndim, ndim))
    for i in range(N):
        if i < N/2 : O += .5 * Z_i(i)
        else : O -= .5 * Z_i(i)
    O2 = np.dot(O, O)
    return 4 * (expectValue(psi, O2) - expectValue(psi, O)**2)
        

h = hamil()
h2 = np.dot(h, h)
w,v = ed(h)
Upool,labelpool = get_Upool()
print(labelpool)
eps = float(sys.argv[2]) # tolerance: 10^-4
niter = int(sys.argv[3]) # max # of steps: 100
skip0 = int(sys.argv[4]) # start line of ini.states
skip1 = int(sys.argv[5]) # end line of ini.states
with open('ini.states') as infile:
    for iline, line in enumerate(infile):
        if iline < skip0 : continue
        if iline >= skip1 : break
        print("initial state: ", line)
        value = line.split()
        psi0 = 1
        for i in range(N):
           angle = np.float_(value[i])  
           p = np.array([np.cos(angle),np.sin(angle)])
           psi0=np.kron(p,psi0)
        theta = []
        Ulist = []
        theta0 = 0
        psiCurr = psi0
        eCurr = costfunc(theta0,np.eye(ndim),psi0,h,h2)
        print('None 0', eCurr)
        for it in range(niter):
            Emin = 1.e6
            for i in range(len(Upool)):
                res = minimize_scalar(costfunc,bounds=(0,np.pi),
                      args=(Upool[i],psiCurr,h,h2), method='Golden',\
                      options={'maxiter':10000})
                if res.fun < Emin:
                    Emin = res.fun
                    iselect = i
                    theta_min = res.x
            print(labelpool[iselect], end=' ')
            theta.append(theta_min)
            Ulist.append(Upool[iselect])
            res = minimize(costfunc2, theta, args=(Ulist,psi0,h,h2), method='nelder-mead',
                           options={'xatol':1e-8,'disp':False, 'maxiter':10000})
            theta = list(res.x)
            eCurr = res.fun
            psiCurr = psi0
            for i in range(len(Ulist)):
                psiCurr = getPsi(Ulist[i], theta[i], psiCurr)
            # check psiCurr with eigenvectors of H
            hpsi = np.dot(h, psiCurr)
            nhpsi = np.linalg.norm(hpsi)
            print(it+1, eCurr)
            if(nhpsi < eps):
                print('reached convergence: ', expectValue(psi0, h).real, 0, it+1, 
                                qfi(psiCurr).real)
                print('finalstate: ', end=' ')
                for i in range(ndim): print(psiCurr[i].real, end=' ')
                print(flush=True)
                break
            else:
                delta = np.absolute(np.vdot(psiCurr,hpsi))/nhpsi - 1
            if(np.absolute(delta) < eps):
                print('reached convergence: ', expectValue(psi0, h).real,
                      expectValue(psiCurr, h).real, it+1, qfi(psiCurr).real)
                print('finalstate: ', end=' ')
                for i in range(ndim): print(psiCurr[i].real, end=' ')
                print(flush=True)
                break
