import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm, rc
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
import sys
import matplotlib.gridspec as gridspec
import pdb

titles = ['hz = 0.0, minimal pool',
          'hz = 0.5, maximal pool']
eigModel1 = 'ED-hz0.0.dat'
eigModel2 = 'ED-hz0.5.dat'
offset = 0

files = ['N6hx0.8hz0.0.out', 'N6hx0.8hz0.5.out']

eig1 = []
eig2 = []
with open(eigModel1) as infile:
    for line in infile:
        values = line.split()
        eig1.append(float(values[0]))
with open(eigModel2) as infile:
    for line in infile:
        values = line.split()
        eig2.append(float(values[0]))

labels = ['(a)', '(b)', '(c)', '(d)']

for i, f in enumerate(files,1):
     energy = []
     clen = []
     l_ = []
     with open(f) as infile:
         for line in infile:
             values = line.split()
             l_.append(float(values[0]))
             energy.append(float(values[1]))
             clen.append(float(values[-1]))
     ave_clen = np.mean(clen)
     plt.subplot(2,2,i)
     #sc = plt.scatter(l_, energy, c=clen,
     #                 vmin=0, vmax=100, s=20)
     sc = plt.scatter(l_, energy, c=clen)
     if i == 2:
         cb = plt.colorbar(sc)
         cb.ax.tick_params(labelsize=16)
     if i ==1:
         for e in eig1:
             plt.plot([min(l_), max(l_)],[e+offset,e+offset], 'gray')

     if i ==2:
         for e in eig2:
             plt.plot([min(l_), max(l_)],[e+offset,e+offset], 'gray')

     plt.xlabel(r'$\lambda$', fontsize=18)
     if i == 1: plt.ylabel('Final energy', fontsize=18)
     #if i == 1: plt.xticks(range(0,len(energy),200),fontsize=18)
     #if i == 1: plt.tick_params(axis = "x",fontsize=18)
     if i == 1: plt.xticks(range( int(min(l_)), int(max(l_))  ,2),fontsize=18)
     if i == 2: plt.xticks(range( int(min(l_)), int(max(l_))  ,2),fontsize=18)
     #ax.xaxis.set_minor_locator(MultipleLocator(50))
     if i==1 : plt.ylim([-8, 10])
     if i==2 : plt.ylim([-8, 12])
     if i == 1 : plt.yticks(np.arange(-8,10,4), fontsize=18)
     if i == 2 : plt.yticks(np.arange(-8,12,4), fontsize=18)
     if i == 1 : plt.text(-7, 8, labels[i-1], fontsize=18)
     if i == 2 : plt.text(-7, 8, labels[i-1], fontsize=18)
     plt.title(titles[i-1], fontsize=18)
     plt.subplot(2,2,i+2)
     xp, bins, p = plt.hist(clen,facecolor='blue',alpha=0.5)
     for item in p : item.set_height(item.get_height()/len(clen))
     plt.xlabel('# of variational parameters', fontsize=18)
     if i == 1: plt.ylabel('Normalized count', fontsize=18)
     plt.ylim((0,0.47))
     plt.xticks(fontsize=18)
     if i == 1 : plt.yticks(fontsize=18)
     if i == 2 : plt.yticks(fontsize=0)
     if i == 1: plt.text(10, 0.42, labels[i+1], fontsize=18)
     if i == 2: plt.text(5, 0.42, labels[i+1], fontsize=18)
     plt.plot([ave_clen, ave_clen],[0,0.47],'g--', linewidth=4.0)

plt.show(block=False)
try:
    input("Hit Enter to close")
except:
    plt.close()


