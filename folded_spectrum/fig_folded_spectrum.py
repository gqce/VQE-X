import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib import cm, rc

#from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import sys

rc('font',**{'family':'sans-serif', 'size' : 10})
rc('text', usetex=True)
#plt.rcParams['text.latex.preamble'] = r'\input{../include/texheader}'
plt.rcParams['text.latex.preamble'] = r'\usepackage{mathrsfs}'

titles = ['$h_z=0$, $\\mathscr{P}_{\\rm min}$',
          '$h_z=0.5$, $\\mathscr{P}_{\\rm max}$']

#eigModel = 'ED-hz0.5.dat'

eigModel1 = 'ED-hz0.0.dat'
eigModel2 = 'ED-hz0.5.dat'

#files = ['vqe-minimal-hz0.5.dat', 'vqe-maximal-hz0.5.dat']
files = ['N6hx0.8hz0.0.out', 'N6hx0.8hz0.5.out']

eig1 = []
eig2 = []
with open(eigModel1) as infile:
    for line in infile:
        values = line.split()
        eig1.append(float(values[0]))
with open(eigModel2) as infile:
    for line in infile:
        values = line.split()
        eig2.append(float(values[0]))

labels = ['(a)', '(b)', '(c)', '(d)']

# one column in APS = 3.375 in
# two column in APS =
fig = plt.figure(figsize = (3.375, 1.5*2.086)) # Golden ratio = 1.62, 3.375/GoldenRatio = 2.086

ax1 = plt.subplot(2,2,1)
ax2 = plt.subplot(2,2,2, sharey = ax1)

# First row: scatter plots
l_min = []
l_max = []
energy_min = []
energy_max = []
clen_min = []
clen_max = []

with open(files[0]) as infile:
	for line in infile:
		values = line.split()
		l_min.append(float(values[0]))
		energy_min.append(float(values[1]))
		clen_min.append(float(values[-1]))
	ave_clen_min = np.mean(clen_min)

with open(files[1]) as infile:
	for line in infile:
		values = line.split()
		l_max.append(float(values[0]))
		energy_max.append(float(values[1]))
		clen_max.append(float(values[-1]))
	ave_clen_max = np.mean(clen_max)

for e in eig1:
	ax1.plot([min(l_min), max(l_min)],[e,e], 'gray', linewidth = 0.5, alpha=0.4)
for e in eig2:
	ax2.plot([min(l_max), max(l_max)],[e,e], 'gray', linewidth = 0.5, alpha=0.4)

sc_min = ax1.scatter(l_min, energy_min, c=clen_min,
                      vmin=0, vmax=100, s=1, alpha = 1.0, cmap='viridis')
sc_max = ax2.scatter(l_max, energy_max, c=clen_max,
                      vmin=0, vmax=100, s=1, alpha = 1.0, cmap='viridis')

cb = plt.colorbar(sc_max, shrink = 1, orientation = 'vertical')
cb.ax.tick_params(labelsize=7)
cb.ax.set_title('$\\mathcal{N}_{\\rm c}$', fontsize = 6)


ax1.set_xlabel('$\\lambda/J$', fontsize = 8)
ax1.set_ylabel('Energy $E/J$', fontsize = 8)
ax2.set_xlabel('$\\lambda/J$', fontsize = 8)

ax1.text(-7,10.5, labels[0], fontsize = 8)
ax2.text(-7,10.5, labels[1], fontsize = 8)

# ax1.text(250, 8, titles[0], fontsize = 8)
# ax2.text(250, 8, titles[1], fontsize = 8)

ax1.set_title(titles[0], fontsize = 8)
ax2.set_title(titles[1], fontsize = 8)

ax1.tick_params(axis = 'x', labelsize = 8)
ax1.tick_params(axis = 'y', labelsize = 8)
#ax1.tick_params(axis = 'x', pad = 8.8)
ax2.tick_params(axis = 'x', labelsize = 8)
ax2.tick_params(axis = 'y', labelsize = 8)

plt.setp(ax2.get_yticklabels(), visible = False)

ax1.set_xticks(np.arange(-8,12,8))
ax1.set_xticks(np.arange(-8,12,4), minor=True)
#ax1.set_xticklabels([r'$-8$','$-4$',r'$\vphantom{-}0$',r'$\vphantom{-}4\vphantom{-}$',r'$\vphantom{-}8\vphantom{-}$'])

ax1.set_ylim([-8, 13])
#ax1.set_xticklabels([r'$-8$']#, r'$-6', r'$-4$, r'$-2$, r'$\phantom{-} 0 \phantom{-}'])
ax1.set_yticks(np.arange(-8,13, 4))

ax2.set_xticks(np.arange(-8,12,8))
ax2.set_xticks(np.arange(-8,12,4), minor=True)

# ax1.set_xticklabels(va = 'baseline')
# ax2.set_xticklabels(va = 'baseline')

# Second row: histograms
ax3 = plt.subplot(2,2,3)
ax4 = plt.subplot(2,2,4, sharey = ax3)

ax3.tick_params(labelsize = 8)
ax4.tick_params(labelsize = 8)
plt.setp(ax4.get_yticklabels(), visible = False)

ax3.set_xticks(np.arange(0,101, 25))
ax4.set_xticks(np.arange(0,101, 25))

ax3.set_xlabel('$\\mathcal{N}_{\\rm c}$', fontsize = 8)
ax3.set_ylabel('Normalized count', fontsize = 8)
ax4.set_xlabel('$\\mathcal{N}_{\\rm c}$', fontsize = 8)

ax3.text(0,0.37, labels[2], fontsize = 8)
ax4.text(0,0.37, labels[3], fontsize = 8)

counts, bins = np.histogram(clen_min, bins = 10, range = (0, 100))
counts = counts/np.sum(counts)

ax3.hist(bins[:-1], bins, weights=counts)

counts, bins = np.histogram(clen_max, bins = 10, range = (0, 100))
counts = counts/np.sum(counts)
ax4.hist(bins[:-1], bins, weights=counts)

ax3.plot([ave_clen_min, ave_clen_min],[0,0.4],'g--', linewidth=1.0)
ax4.plot([ave_clen_max, ave_clen_max],[0,0.4],'g--', linewidth=1.0)

plt.tight_layout()
#plt.show()
plt.savefig('./fig_folded_spectrum.png', format='png', dpi = 600, bbox_inches='tight')
